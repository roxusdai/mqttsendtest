package com.ble.mqttexample

import MAVLink.MAVLinkPacket
import MAVLink.Messages.MAVLinkMessage
import MAVLink.Parser
import MAVLink.bluetooth.msg_connect
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity

//import com.ble.mqttexample.MqttClass


import MAVLink.mavlink_main.*
import MAVLink.bootloader.*
import MAVLink.smartmattress.*
import MAVLink.logger.*
import MAVLink.bluetooth.*
import MAVLink.common.msg_radio_status
import android.widget.*
import org.eclipse.paho.client.mqttv3.MqttMessage
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {
    lateinit var bt_sub : Button
    lateinit var sp_msg : Spinner
    lateinit var et_wifimac : EditText

    lateinit var et_arg1 : EditText
    lateinit var et_arg2 : EditText
    lateinit var et_arg3 : EditText
    lateinit var et_arg4 : EditText

    var mqttclass : MqttClass? = MqttClass()
    var subornot = false
    //val MQTT_BROKER_URL = "tcp://broker.emqx.io:1883"

    val serverURL = "tcp://114.34.221.116:6673"
    val mqttuser  = "smartmattress"
    val mqttpwd =   "aRkZQwD4"

    val topic1 = "smttrss"

    val topic2 = arrayOf("control", "sensor", "ack", "err", "config", "model", "ota", "else")

    //val topic3 = "7c:df:a1:c2:13:ca"   ///for 5f ble mac
    //val topic3 = "7c:df:a1:c2:13:c8"  ///for 5f wifi mac


    var topic3 =    "7c:df:a1:c2:96:ae" ////for danny device ble mac
    //val topic3 =  "7c:df:a1:c2:96:ac" ////danny device wifi


    ///val topic3 = "7C:DF:A1:B8:85:58"////for test

    ///var mavpac :MAVLinkPacket = MAVLinkPacket(8 ,false)
    /////to encode, mavpac.encodePacket()
    val bytemessage = ""

    var msgid = msg_bl_command.MAVLINK_MSG_ID_BL_COMMAND
    var bed_lrb = 0
    var position = 0x01.toByte()

    /*
    val msglist = arrayListOf<Int>(
            msg_bl_ack.MAVLINK_MSG_ID_BL_ACK,
            msg_bl_command.MAVLINK_MSG_ID_BL_COMMAND,
            msg_bl_ota.MAVLINK_MSG_ID_BL_OTA,
            msg_logger_code.MAVLINK_MSG_ID_LOGGER_CODE,
            msg_logger_msg.MAVLINK_MSG_ID_LOGGER_MSG,
            msg_ble_ack.MAVLINK_MSG_ID_BLE_ACK,
            msg_bluetooth_change_name.MAVLINK_MSG_ID_BLUETOOTH_CHANGE_NAME,
            msg_wifi_set_ssid_password.MAVLINK_MSG_ID_WIFI_SET_SSID_PASSWORD,
            msg_mqtt_set_ip_password.MAVLINK_MSG_ID_MQTT_SET_IP_PASSWORD,
            msg_connect.MAVLINK_MSG_ID_CONNECT,
            msg_mattress_ack.MAVLINK_MSG_ID_MATTRESS_ACK,
            msg_adjust_hardness.MAVLINK_MSG_ID_ADJUST_HARDNESS,
            msg_relieve_stress.MAVLINK_MSG_ID_RELIEVE_STRESS,
            msg_meditation.MAVLINK_MSG_ID_MEDITATION,
            msg_connection.MAVLINK_MSG_ID_CONNECTION,
            msg_pressure.MAVLINK_MSG_ID_PRESSURE,
            msg_pump_status.MAVLINK_MSG_ID_PUMP_STATUS,
            msg_step_status.MAVLINK_MSG_ID_STEP_STATUS,
            msg_request_data.MAVLINK_MSG_ID_REQUEST_DATA,
            msg_control_pump.MAVLINK_MSG_ID_CONTROL_PUMP,
            msg_control_step.MAVLINK_MSG_ID_CONTROL_STEP,
            msg_radio_status.MAVLINK_MSG_ID_RADIO_STATUS
    )
    */

    val msglist = arrayListOf<Int>(
            msg_bl_command.MAVLINK_MSG_ID_BL_COMMAND,
            msg_bl_ota.MAVLINK_MSG_ID_BL_OTA,
            msg_connect.MAVLINK_MSG_ID_CONNECT,
            msg_logger_code.MAVLINK_MSG_ID_LOGGER_CODE,
            msg_logger_msg.MAVLINK_MSG_ID_LOGGER_MSG,
            msg_bluetooth_change_name.MAVLINK_MSG_ID_BLUETOOTH_CHANGE_NAME,
            msg_wifi_set_ssid_password.MAVLINK_MSG_ID_WIFI_SET_SSID_PASSWORD,
            msg_mqtt_set_ip_password.MAVLINK_MSG_ID_MQTT_SET_IP_PASSWORD,
            msg_connect.MAVLINK_MSG_ID_CONNECT,
            msg_adjust_hardness.MAVLINK_MSG_ID_ADJUST_HARDNESS,
            msg_relieve_stress.MAVLINK_MSG_ID_RELIEVE_STRESS,
            msg_meditation.MAVLINK_MSG_ID_MEDITATION,
            msg_connection.MAVLINK_MSG_ID_CONNECTION,/////smartmatress
            msg_control_pump.MAVLINK_MSG_ID_CONTROL_PUMP,
            msg_control_step.MAVLINK_MSG_ID_CONTROL_STEP
    )
    val msgnamelist = arrayListOf<String>(
            "MAVLINK_MSG_ID_BL_COMMAND",
            "MAVLINK_MSG_ID_BL_OTA",
            "MAVLINK_MSG_ID_CONNECT",
            "MAVLINK_MSG_ID_LOGGER_CODE",
            "MAVLINK_MSG_ID_LOGGER_MSG",
            "MAVLINK_MSG_ID_BLUETOOTH_CHANGE_NAME",
            "MAVLINK_MSG_ID_WIFI_SET_SSID_PASSWORD",
            "MAVLINK_MSG_ID_MQTT_SET_IP_PASSWORD",
            "MAVLINK_MSG_ID_CONNECT",
            "MAVLINK_MSG_ID_ADJUST_HARDNESS",
            "MAVLINK_MSG_ID_RELIEVE_STRESS",
            "MAVLINK_MSG_ID_MEDITATION",
            "MAVLINK_MSG_ID_CONNECTION",/////smartmatress
            "MAVLINK_MSG_ID_CONTROL_PUMP",
            "MAVLINK_MSG_ID_CONTROL_STEP"
    )


    // TAG
    companion object {
        const val TAG = "AndroidMqttClient"
    }

    fun mqttconnect(view: View) {
        topic3 = et_wifimac.text.toString()
        mqttclass!!.connect(this, serverURL, mqttuser, mqttpwd)
    }
    fun mqttdisconnect(view: View) {
        mqttclass!!.disconnect()
    }

    fun mqttpublish(view: View) {
        val arg1 = et_arg1.text.toString().toShort()
        val arg2 = et_arg2.text.toString().toShort()
        val arg3 = et_arg3.text.toString().toShort()
        val arg4 = et_arg4.text.toString().toShort()


        var publishmsg : MqttMessage?= MqttMessage()
        var thistopic = ""
        publishmsg!!.setId(msgid)
        when(msgid){
            msg_bl_command.MAVLINK_MSG_ID_BL_COMMAND->{

            }//////2
            msg_bl_ota.MAVLINK_MSG_ID_BL_OTA->{

            }/////3
            msg_connect.MAVLINK_MSG_ID_CONNECT->{
                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3
                val mavpac = msg_connect(arg1)
                publishmsg!!.setPayload(mavpac.pack().encodePacket())

            }/////34

            ////for smart ress
            msg_adjust_hardness.MAVLINK_MSG_ID_ADJUST_HARDNESS->{

                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3

                val mavpac = msg_adjust_hardness(arg1,arg2 , arg3,arg4 )

                publishmsg!!.setPayload(mavpac.pack().encodePacket())

            }/////52
            msg_relieve_stress.MAVLINK_MSG_ID_RELIEVE_STRESS->{

                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3
                val mavpac = msg_relieve_stress(arg1,arg2,arg3,arg4)
                publishmsg!!.setPayload(mavpac.pack().encodePacket())

            }/////53

            msg_meditation.MAVLINK_MSG_ID_MEDITATION->{
                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3
                val mavpac = msg_meditation(arg1,arg2,arg3,arg4)
                publishmsg!!.setPayload(mavpac.pack().encodePacket())
            }/////54

            msg_connection.MAVLINK_MSG_ID_CONNECTION->{
                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3
                val mavpac = msg_connection(arg1,arg2,arg3)
                publishmsg!!.setPayload(mavpac.pack().encodePacket())


            }/////55

            msg_request_data.MAVLINK_MSG_ID_REQUEST_DATA->{
                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3
                val mavpac = msg_request_data(arg1)
                publishmsg!!.setPayload(mavpac.pack().encodePacket())
            }/////59

            msg_control_pump.MAVLINK_MSG_ID_CONTROL_PUMP->{
                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3
                val mavpac = msg_control_pump(arg1.toInt())
                publishmsg!!.setPayload(mavpac.pack().encodePacket())
            }/////60

            msg_control_step.MAVLINK_MSG_ID_CONTROL_STEP->{
                thistopic = topic1 + "/"  + topic2[0] + "/" + topic3
                val mavpac = msg_control_step(arg1.toInt(),arg2)
                publishmsg!!.setPayload(mavpac.pack().encodePacket())

            }////61

        }


        if (publishmsg != null) {
            mqttclass!!.publish(thistopic, publishmsg , 1)
        }
    }
    fun mqttsub(view: View) {
        mqttclass!!.subscribe("#", 1)
    }
    fun mqttunsub(view: View){
        mqttclass!!.unsubscribe("#")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bt_sub = findViewById(R.id.button2)
        sp_msg = findViewById(R.id.msgspinner)
        et_wifimac = findViewById(R.id.et_wifi_mac)
        et_wifimac.setText(topic3)

        et_arg1 = findViewById(R.id.et_arg1)
        et_arg2 = findViewById(R.id.et_arg2)
        et_arg3 = findViewById(R.id.et_arg3)
        et_arg4 = findViewById(R.id.et_arg4)


        val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                msgnamelist
        )

        sp_msg.adapter = adapter
        sp_msg.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Log.e("Spinner", "select "+ msglist[p2].toString() )
                msgid = msglist[p2].toInt()
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }


    }

    fun mqttsubornot(view: View) {
        if(subornot){
            subornot = false
            bt_sub.text = "subscribe"
            mqttunsub(view)
        }///sub to unsub
        else{
            subornot = true
            bt_sub.text = "unsubscribe"
            mqttsub(view)
        }////unsub to sub

    }


    fun parsetest(view: View) {
        /*
        val packet = MAVLinkPacket(3)
        while(!packet.payloadIsFilled()){
            packet.payload.add(0x02)
        }
        */

        val packet = msg_connect(3).pack()

        Log.d("parsetest", packet.payload.data.toString())
        val result_packet = packet.encodePacket()

        var mvlnk_partest = Parser()

        var parsertmp :MAVLinkPacket ?= null

        for(i in result_packet){
            val res = mvlnk_partest.mavlink_parse_char(i.toInt())
            if(res != null)parsertmp = res
        }
        Log.d("parsetest", "Receive: ${parsertmp!!.payload.payload.array()}")
    }

    fun clickexit(view: View) {
        moveTaskToBack(true);
        exitProcess(-1)
    }


}